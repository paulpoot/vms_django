# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0002_auto_20151127_1012'),
    ]

    operations = [
        migrations.AlterField(
            model_name='vehicle',
            name='unitid',
            field=models.BigIntegerField(unique=True),
        ),
    ]
