# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0003_auto_20151127_1022'),
    ]

    operations = [
        migrations.AddField(
            model_name='vehicle',
            name='comments',
            field=models.TextField(blank=True, default=''),
        ),
        migrations.AddField(
            model_name='vehicle',
            name='favorite',
            field=models.BooleanField(default=False),
        ),
    ]
