from django.contrib import admin
from website.models import Vehicle

class VehicleAdmin(admin.ModelAdmin):
    list_display = ('unitid', 'favourite')

# Register your models here.
admin.site.register(Vehicle, VehicleAdmin)
