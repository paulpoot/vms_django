from django.db import models

# Create your models here.
class Vehicle(models.Model):
    unitid = models.BigIntegerField(unique=True)
    favourite = models.BooleanField(default=False)
    comments = models.TextField(blank=True, default='There are no comments about this vehicle yet.')

    def __str__(self):
        return str(self.unitid)
