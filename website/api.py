import json
import requests

def run_query(type, unitid):

    # Base URL
    root_url = "https://vms-hro.janhagenaar.eu/api/"

    if type == "ProcessorUsage":
        data_type = "Monitoring/GetProcessorUsageInfo"
    elif type == "MemoryUsage":
        data_type = "Monitoring/GetMemoryUsageInfo"
    elif type == "GPSTemperature":
        data_type = "Monitoring/GetGpsInfo"
    else:
        print("Error: Invalid type input.")


    request_url = root_url + data_type + "?unitId=" + str(unitid)

    results = []

    try:
        # Connect
        response = requests.get(request_url)

        # Convert response to dictionary object
        json_response = response.json()

        # Loop through results
        if type == "ProcessorUsage":
            for result in json_response['ProcessorUsage']:
                results.append({
                    'DateTime': result['DateTime'],
                    'Usage': result['Usage']
                })
            results.append({
                'Usage': json_response['AverageUsage']
            })

        if type == "MemoryUsage":
            for result in json_response['MemoryLoad']:
                results.append({
                    'DateTime': result['DateTime'],
                    'Usage': result['Usage']
                })
            results.append({
                'Usage': json_response['AverageLoad']
            })

        if type == "GPSTemperature":
            for result in json_response['GpsTemperatureInfoModels']:
                results.append({
                    'DateTime': result['DateTime'],
                    'Temperature': result['Temperature']
                })
            results.append({
                'Temperature': json_response['AverageTemperature']
            })

    # Catch URLError exception
    except requests.exceptions.RequestException, e:
        print "Error querying API: ", e

    return results

def get_processorusage(unitid):
    usage = []
    datetime = []

    data = run_query("ProcessorUsage", unitid)
    average = data[ len(data) - 1 ]['Usage']

    for result in range(0, len(data) - 1):
        usage.append( data[result]['Usage'] )
        datetime.append( str(data[result]['DateTime'])[5:-3] )

    processorusage = {
        'Usage': usage,
        'DateTime': datetime,
        'Average': average,
    }

    return processorusage

def get_memoryusage(unitid):
    usage = []
    datetime = []

    data = run_query("MemoryUsage", unitid)
    average = data[ len(data) - 1 ]['Usage']

    for result in range(0, len(data) - 1):
        usage.append( data[result]['Usage'] )
        datetime.append( str(data[result]['DateTime'])[5:-3] )

    memoryusage = {
        'Usage': usage,
        'DateTime': datetime,
        'Average': average,
    }

    return memoryusage

def get_gpstemperature(unitid):
    temperature = []
    datetime = []

    data = run_query("GPSTemperature", unitid)
    average = data[ len(data) - 1 ]['Temperature']

    for result in range(0, len(data) - 1):
        temperature.append( data[result]['Temperature'] )
        datetime.append( str(data[result]['DateTime'])[5:-3] )

    gpstemperature = {
        'Temperature': temperature,
        'DateTime': datetime,
        'Average': average,
    }

    return gpstemperature

if __name__ == '__main__':
    print "Connecting with API..."
    run_query("ProcessorUsage", 14100064)
