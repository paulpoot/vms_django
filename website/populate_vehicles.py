import json
import requests

import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'vms.settings')

import django
django.setup()

from website.models import Vehicle

def populate():

    url = "https://vms-hro.janhagenaar.eu/api/Position/GetUniqueUnitIds"

    try:
        # Connect
        response = requests.get(url)

        # Convert response to dictionary object
        json_response = response.json()

        # Loop through results
        for result in json_response:
            add_vehicle(
              unitid = result,
              favourite = 0,
              comments = "There are no comments about this vehicle yet.",
            )

    # Catch URLError exception
    except requests.exceptions.RequestException, e:
        print "Error querying API: ", e

    # Print the data we just added to the user
    for v in Vehicle.objects.all():
        print("Vehicle: " + str(v))

def add_vehicle(unitid, favourite, comments):
    v = Vehicle.objects.get_or_create(unitid=unitid)[0]
    v.favourite=favourite
    v.comments=comments
    v.save()
    return v

# Start execution
if __name__ == '__main__':
    print("Starting Vehicle Population Script.")
    populate()
