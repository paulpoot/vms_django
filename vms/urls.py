"""vms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

from website import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^vehicles/(?P<vehicleid>[0-9]+)/$', views.vehicle),
    url(r'^vehicles/search/', views.search_vehicles),
    url(r'^vehicles/refresh/', views.populate_vehicles),
    url(r'^vehicles/favourite/(?P<vehicleid>[0-9]+)/(?P<update>[A-Z][a-z]+)/$', views.update_favourite),
    url(r'^vehicles/comment/(?P<vehicleid>[0-9]+)/$', views.update_comment),
    url(r'^vehicles/$', views.vehicle_overview),
    url(r'^login/', views.user_login),
    url(r'^accounts/login/', views.user_login),
    url(r'^logout/', views.user_logout),
    url(r'^admin/', admin.site.urls),
]