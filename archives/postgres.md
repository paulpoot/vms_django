# Postgres
---

## Backup and Restore
Before you can create or restore a backup, you need to follow these steps:

1. Enter the /vagrant/vms_django folder.
2. Login as Postgres user. For reference: `sudo su postgres`

### Restore backup
Execute the `psql djangodb < db_backup.sql` command.

### Create backup
Execute the `pg_dump djangodb > db_backup.sql` command.

---

## Contents old Word document for reference
**Installing all packages you need**

- First perform an apt-get update on your server to avoid any problems with versions.  
`sudo apt-get update`

- Then you have to install some python stuff (which you probably already have) plus postgres and some libs.  
  `sudo apt-get install python-pip python-dev libpq-dev postgresql postgresql-contrib`

**Create the database and a database user**

Installing the Postgres stuff created a user for you named “postgres”
- Swap to the user account “postgres”  
  `sudo su – postgres`  
Now you are logged in as the “postgres” user and you can open a sql shell to the database.
- Open the shell.  
  `psql`
- Create the database.   
  `CREATE DATABASE djangodb;`
- Create a user for the database.  
  `CREATE USER hhhhh WITH PASSWORD ‘root’;`  
Then we have to adjust some default settings because Django will cry if we don’t.  
  `ALTER ROLE hhhhh SET client_encoding TO ‘utf8’;`  
  `ALTER ROLE hhhhh SET default_transaction_isolation TO ‘read committed’;`  
  `ALTER ROLE hhhhh SET timezone TO ‘UTC’;`  
The last thing we have to do is give full permission over the database to the user.  
  `GRANT ALL PRIVILEGES ON DATABASE djangodb TO hhhhh;`
You can now exit the shell and logout from the user.  
  `\q`  
  `exit`

**Configuring the Django part**  
Since we already have the Django part we only have to adjust a few things.  

First to have to enter your django_project (with workon and stuff) and install the following:  
  `pip install django psycopg2`  
Now you have to migrate your database and add a super user.  
  `python manage.py makemigrations`  
  `python manage.py migrate`  
  `python manage.py createsuperuser (username:hhhhh, email:, password:root)`  

Now all we have to do is edit the settings file. Change the DATABASES section to the following:
`'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'djangodb',
        'USER': 'hhhhh',
        'PASSWORD': 'root',
        'HOST': 'localhost',
        'PORT': '',
    }`

Should be working now!
