## Processor Usage
https://vms-hro.janhagenaar.eu/api/Monitoring/GetProcessorUsageInfo?unitId=14100064

## GPS Temperature
https://vms-hro.janhagenaar.eu/api/Monitoring/GetGpsInfo?unitId=14100064

## Unit IDs
https://vms-hro.janhagenaar.eu/api/Position/GetUniqueUnitIds
